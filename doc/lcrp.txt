Name

   lcrp — calculate the Lipschitz constant k(p) for the projection onto
   the unit ball of p-norm function and sequence spaces.

Synopsis

#include <lcrp.h>

   int lcrp( double p,
             double * C);

   const char * lcrp_strerror( int errorcode);

DESCRIPTION

   The lcrp function takes an double argument p (which should be between
   one and INFINITY inclusive) and assigns the Lipschitz constant k(p) to
   the double whose address is given by the parameter C.

RETURN VALUE

   On success, the function returns zero (LCRP_OK) and assigns the
   parameter C.

   On failure, the function returns a non-zero value and does not assign
   C. The return value can be passed to the function lcrp_strerror to
   obtain a description of the error.

AUTHOR

   J.J. Green
