VERSION = 1.0

LIB = liblcrp.a
OBJ = lcrp.o

default : all

include Common.mk

all : $(LIB)

$(LIB) : $(OBJ)
	ar rs $(LIB) $(OBJ)

test : $(LIB)
	$(MAKE) -C test test

doc :
	$(MAKE) -C doc all

clean :
	$(RM) $(LIB) $(OBJ)
	$(MAKE) -C test clean
	$(MAKE) -C doc clean

DIST = lcrp-$(VERSION)

dist : clean
	cd .. ; \
	cp -pr lcrp $(DIST) ;\
	tar --exclude-from=$(DIST)/.distignore -zpcvf $(DIST).tar.gz $(DIST) ;\
	mv $(DIST) /tmp

.PHONY : default test doc clean dist
