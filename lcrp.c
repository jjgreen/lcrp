/*
  lcrp.c

  Implements the double versions of lcrp()

  Copyright (c) 2011, 2012, 2015,  J.J. Green
  MIT License
*/

#include <errno.h>
#include <math.h>
#include <stdlib.h>
#include <float.h>

#include "lcrp.h"

#define XASYMP0 1e-9
#define XASYMP1 1e-3
#define ITERATIONS 20

static double initial(double);

static double f_eval_large(double, double, double, double);
static double f_eval_small(double, double, double, double);

int lcrp(double p, double *C)
{
  if (p < 1)
    {
      errno = EDOM;
      return LCRP_EDOM;
    }

  if (isinf(p) || (p == 1.0))
    {
      *C = 2.0;
      return LCRP_OK;
    }

  if (p == 2.0)
    {
      *C = 1.0;
      return LCRP_OK;
    }

  double x;

  if (p > 2)
    {
      x = 1 / (p - 1);
      p = x + 1;
    }
  else
    x = p - 1;

  if (x < XASYMP0)
    {
      *C = 2 + x * (log(x / 8) - 1);
      return LCRP_OK;
    }

  if (1 - x < XASYMP1)
    {
      double
	y  = 1 - x,
	c2 = 0.21961441994532257538,
	c4 = 0.15444802887153513329;

      *C = 1 + y * y * (c2 + y * (c2 + y * c4));
      return LCRP_OK;
    }

  double (*f_eval)(double, double, double, double);

  if (x > 0.9)
    f_eval = f_eval_large;
  else
    f_eval = f_eval_small;

  double u = initial(x);

  double
    v = u * x,
    A = pow(v, x),
    B = pow(v, 1 / x),
    x2 = x * x,
    x3 = x2 * x,
    xp = x * p;

  for (size_t i = 0 ; i < ITERATIONS ; i++)
    {
      double
	xv = x * v,
	df = (x3 - p * (xv - B)) * A / v
             + (1 - p * (xv - x2 * A)) * B / v
             - xp;

      if (df == 0) return LCRP_NR_ZD;

      double
	f = f_eval(x, v, A, B),
	du = -f / df;

      u += du;

      if ((u <= 0) || (u >= 1)) return LCRP_NR_ESC;

      v = u * x;
      A = pow(v, x);
      B = pow(v, 1 / x);

      if ((fabs(du) < u * DBL_EPSILON) ||
	  (fabs(f) < x * (1 - x) * sqrt(DBL_EPSILON)))
	{
	  *C = (pow((1 + A) * exp(x * log1p(B)), 1 / p)) / (1 + v);
	  return LCRP_OK;
	}
    }

  return LCRP_NR_NOCONV;
}

static double f_eval_large(double x, double v, double A, double B)
{
  double
    L = log(v),
    C = expm1((1 / x - 1) * L),
    D = expm1((x - 1) * L);

  return  v * ((1 + A) * C + x * (1 + B) * D);
}

static double f_eval_small(double x, double v, double A, double B)
{
  return (1 + A) * (B - v) + x * (1 + B) * (A - v);
}

static double chebeval(size_t, const double[static 3], double);

static double initial(double x)
{
  const double a[] = {
    -2.5595939371497115e-04,
    +7.0686844943148523e-04,
    -1.2561575232566727e-03,
    +1.8047998427177896e-03,
    -1.5614528129381411e-03,
    +1.3075424887128736e-03,
    -1.0205255490737759e-02,
    +5.6520272597365064e-02,
    -1.9133303566591867e-01,
    +2.3504865573520661e-01
  };
  size_t n = sizeof(a) / sizeof(double);

  return chebeval(n, a, 2 * x - 1);
}

static double chebeval(size_t n, const double a[static 3], double x)
{
  double b[3], z = 2 * x;

  b[0] = a[0];
  b[1] = z * b[0] + a[1];

  for (size_t j = 0 ; j < n - 2 ; j++)
    b[(j + 2) % 3] = z * b[(j + 1) % 3] + a[j + 2] - b[j % 3];

  return x * b[(n - 2) % 3] + a[n - 1] - b[(n - 3) % 3];
}

const char* lcrp_strerror(int err)
{
  static const char nomsg[] = "unimplemented message";

  struct
  {
    int err;
    const char *msg;
  } *m, mtab[] =
      {
	{ LCRP_OK,        "Success" },
	{ LCRP_EDOM,      "Argument out of domain" },
	{ LCRP_NR_ZD,     "Newton-Raphson zero derivative" },
	{ LCRP_NR_ESC,    "Newton-Raphson escape" },
	{ LCRP_NR_NOCONV, "Newton-Raphson did not converge" },
	{ -1, NULL}
      };

  for (m = mtab ; m->msg ; m++)
    if (m->err == err) return m->msg;

  return nomsg;
}
