#! /usr/bin/python
#
# reads lines of p, k(p) pairs (where p in in hexadecimal
# floating point format, printf("%a") output, k(p) in
# standard floating point format), calculates k(p)
# to high precision using the bisection method with the
# python mpmath multiprecision library and prints the
# relative accuracy of the input.  This is not fast.

from mpmath import *

def bisect(x):

    u0 = mpf('0')
    u1 = mpf('1')
    u = mpf('0.5')
    sf0 = 1
    sf1 = -1

    du = power(10, -mp.dps)

    while fsub(u1, u0) > du:

        ux = fmul(u, x)
        A  = power(ux, x)
        B  = power(ux, fdiv(1, x))

        C1 = fmul(fadd(1, A), fsub(B, ux))
        C2 = fmul(x, fmul(fadd(1, B), fsub(A, ux)))
        f  = fadd(C1, C2)
        sf = sign(f)

        if sf * sf0 > 0:
            u0 = u
            sf0 = sf
        elif sf * sf1 > 0:
            u1 = u
            sf1 = sf
        else:
            return u

        u = fdiv(fadd(u0, u1), 2)

    return u

def lcrp(x):

    u = bisect(x)

    v = fmul(u, x)
    A = power(v, x);
    B = power(v, fdiv(1, x));
    p = fadd(x, 1)

    C1 = power(fadd(1, A), fdiv(1, p))
    C2 = power(fadd(1, B), fdiv(x, p))
    C3 = fadd(1, v)

    return fdiv(fmul(C1, C2), C3)

mp.dps = 50

import fileinput

for line in fileinput.input():

    ps, kps = line.split()
    pf = float.fromhex(ps)
    kpf = float(kps)
    kp0 = mpf(kpf)
    x = fsub(mpf(pf), 1)
    kp1 = lcrp(x)
    acc = fdiv(fsub(kp1, kp0), kp1)
    print "%.15f %.10e" % (pf, acc)
