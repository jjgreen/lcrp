#include <lcrp.h>

#include "tests_lcrp_strerror.h"
#include "tests_helper.h"

CU_TestInfo tests_lcrp_strerror[] =
  {
    {"OK is zero",                     test_lcrp_strerror_ok},
    {"message for LCRP_OK",            test_lcrp_strerror_msg_ok},
    {"message for unknown error code", test_lcrp_strerror_msg_unknown},
    CU_TEST_INFO_NULL,
  };

extern void test_lcrp_strerror_ok(void)
{
  CU_ASSERT_EQUAL(LCRP_OK, 0);
}

extern void test_lcrp_strerror_msg_ok(void)
{
  const char *msg = lcrp_strerror(LCRP_OK);

  CU_ASSERT_PTR_NOT_NULL_FATAL(msg);
  CU_ASSERT_STRING_EQUAL(msg, "Success");
}

extern void test_lcrp_strerror_msg_unknown(void)
{
  const char *msg = lcrp_strerror(12345);

  CU_ASSERT_PTR_NOT_NULL_FATAL(msg);
  CU_ASSERT_STRING_EQUAL(msg, "unimplemented message");
}
