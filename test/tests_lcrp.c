/*
  tests_lcrp.h

  Copyright (c) J.J. Green 2015
*/

#include <lcrp.h>
#include <float.h>
#include <math.h>
#include <errno.h>

#include "tests_lcrp.h"
#include "tests_helper.h"

CU_TestInfo tests_lcrp[] =
  {
    {"known exact values",  test_lcrp_known},
    {"conjugate symmetry",  test_lcrp_conjugate},
    {"domain error",        test_lcrp_edom},
    {"asymtotics near 1",   test_lcrp_asymp_one},
    {"asymtotics near 2",   test_lcrp_asymp_two},
    CU_TEST_INFO_NULL,
  };

#define TEST_EPS (1.5 * DBL_EPSILON)

/*
  Check some values of lcrp which are known exactly

  O.P. Kapoor and S.B. Mathur, "Metric projection bound
  and the Lipschitz constant of the radial retraction",
  J. Approx. Theory 38 (1), 1983, pp. 66-70.
*/

static void lcrp_known(double p, double C)
{
  double C0;
  int err = lcrp(p, &C0);

  CU_ASSERT_EQUAL(err, LCRP_OK);
  CU_ASSERT_DOUBLE_EQUAL(C, C0, TEST_EPS * C);
}

extern void test_lcrp_known(void)
{
  lcrp_known(1, 2);
  lcrp_known(2, 1);
  lcrp_known(3, pow(17 + 7*sqrt(7), 1/3.0)/3);
  lcrp_known(4, pow(1 + 2*sqrt(3)/3, 1/4.0));
  lcrp_known(INFINITY, 2);
}

/* if 1/p + 1/q = 1, then lcpr of p and q are equal */

extern void test_lcrp_conjugate(void)
{
  double p[] = {1.01, 1.1, 1.2, 1.3, 1.5, 1.9, 1.99};
  size_t n = sizeof(p)/sizeof(double);

  for (size_t i = 0 ; i < n ; i++)
    {
      double pi = p[i], qi = 1 / (1 - 1/pi);
      double Cp, Cq;

      CU_ASSERT_EQUAL(lcrp(pi, &Cp), LCRP_OK);
      CU_ASSERT_EQUAL(lcrp(qi, &Cq), LCRP_OK);
      CU_ASSERT_DOUBLE_EQUAL(Cp, Cq, TEST_EPS * Cp);
    }
}

/* domain error returns LCRP_EDOM and sets errno to EDOM */

extern void test_lcrp_edom(void)
{
  double p = 0.5, C = 0.0;
  errno = 0;

  CU_ASSERT_EQUAL(lcrp(p, &C), LCRP_EDOM);
  CU_ASSERT_EQUAL(C, 0.0);
  CU_ASSERT_NOT_EQUAL(errno, 0);
  CU_ASSERT_EQUAL(errno, EDOM);
}

/* Equation (9) of the unpublished technical note */

extern void test_lcrp_asymp_one(void)
{
  double
    x  = 1e-10,
    p  = 1 + x,
    C0 = 2 + x * ( log(x/8) - 1 ), C1;

  CU_ASSERT_EQUAL(lcrp(p, &C1), LCRP_OK);
  CU_ASSERT_DOUBLE_EQUAL(C0, C1, TEST_EPS * C0);
}

/* Equation (9) of the unpublished technical note */

extern void test_lcrp_asymp_two(void)
{
  double
    y  = 1e-4,
    p  = 2 - y,
    A  = 0.21961441994532257538,
    B  = 0.13751213124566818941,
    C0 = 1 +  y * y * (A + y * (A + y * B)), C1;

  CU_ASSERT_EQUAL(lcrp(p, &C1), LCRP_OK);
  CU_ASSERT_DOUBLE_EQUAL(C0, C1, TEST_EPS * C0);
}
